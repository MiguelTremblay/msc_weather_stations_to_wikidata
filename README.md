MSC Weather Stations to Wikidata : Convert Meteorological Service of Canada (MSC) weather stations metadata into Wikidata input format (QuickStatements)
=============

Introduction
------------

Python3 script converting the list of [Meteorological Service of Canada weather historical station](http://climate.weather.gc.ca/historical_data/search_historic_data_e.html) metadata into [QuickStatements](https://www.wikidata.org/wiki/Help:QuickStatements) Wikidata input format .

Requirements
------------

* [python3](https://www.python.org/downloads/) 

___

Download
--------
The latest version can be downloaded here:<br>
https://framagit.org/MiguelTremblay/msc_weather_stations_to_wikidata

The git version can be accessed here:<br>
 ```git clone https://framagit.org/MiguelTremblay/msc_weather_stations_to_wikidata```


Manual
--------

In a general way, this software should be called in command line like this:
```bash
python msc_weather_stations_to_wikidata.py [OPTIONS] 
```
<br />
where:
* OPTIONS are described in the table below.


| Options                                  | Description |
| -------                                  | ------------|
| `-h`, `--help`                           | Show help message and exit.|
| `-N` `--online`                          | Download the station list from the MSC Climate web site for [French version](ftp://client_climate@ftp.tor.ec.gc.ca/Pub/Get_More_Data_Plus_de_donnees/R%E9pertoire%20des%20stations%20FR.csv) and [English version](ftp://client_climate@ftp.tor.ec.gc.ca/Pub/Get_More_Data_Plus_de_donnees/Station%20Inventory%20EN.csv) |
|`-f` `--station-file-fr`&nbsp;PATH        | Use the file for the station list in French located at PATH on your local computer |
| `-e` `--station-file-en`&nbsp;PATH       | se the file for the station list in English located at PATH on your local computer |
| `-o` `--output-file`                     | Output the information in the output file instead of stdout.
| `-w` `--wikidata-file` file.csv          | Provide a CSV file with the stations and properties already existing in Wikidata. See section __Elements file__ below for more information about the format and content.|
| `-p` `--overwrite-property` PROPERTY [...]  | Overwrite the value(s) of element(s) given in argument, even if there is already a value provided in the __Elements file__. This option can only be used combined with the option `--wikidata-file`. If the value 'all' is provided, all the properties are overwritten. |
| `-q` `--station-only` QNUMBER [...]      | Provide values only for stations identified by their elements values (Q1, Q2, etc.). If the value 'all' is provided, only the stations existing in the __Elements file__ will be provided in the output. This option can only be used combined with the option `--wikidata-file`. |
| `-d` `--delete` PROPERTY [...]           | Delete the property listed with `-p` in the QS instruction.
| `-l` `--list-properties`                 | List all the property values assigned by this script and their corresponding description, then exit. |
|`-v` `--verbose`                          | Explain what is being done.|
|`-V` `--version`                          | Output version information and exit.|


Elements file
-----

This file is used to prevent the creation of a new element each time the QuickStatement tool is used. It is a CSV export of [this Wikidata Query Service request](https://www.wikidata.org/wiki/Wikidata:WikiProject_Weather_observations/en/SPARQL#Request_for_update   ).

Here's an example of output:<br>
> station,label_en,description_en,label_fr,description_fr,instance_of,coordinate_location,province_label,identifiant_WIGOS_de_station,property_of,start_date,end_date,msc_climate_id,altitude,data_interval<br>
> http://www.wikidata.org/entity/Q60766161,Arthurette Birch Ridge,"Meteorogical Service of Canada's station for Arthurette Birch Ridge, New Brunswick, Canada",Arthurette Birch Ridge,"station météorologique du Service météorologique du Canada pour Arthurette Birch Ridge, Nouveau-Brunswick, Canada",http://www.wikidata.org/entity/Q190107,Point(-67.47 46.75),http://www.wikidata.org/entity/Q1965,,http://www.wikidata.org/entity/Q349450,1967-01-01T00:00:00Z,1983-01-01T00:00:00Z,8100350,214.9,http://www.wikidata.org/entity/Q59657036

When an item contains a value, this value is not updated by the script. If one wants this value to be updated, they only have to erase the value in the table and run the script. See the options for more information.

Usage
-----

Command for regular update:
```bash
  python3 msc_weather_stations_to_wikidata.py -f station_list/Station\ Inventory\ FR.csv -e station_list/Station\ Inventory\ EN.csv  -w query.csv -o quickstatements.txt
```
<br />


Use online version of station list, the CSV file _element_stations.csv_ for elements already in Wikidata ( [see this request to get this file](https://www.wikidata.org/wiki/Wikidata:WikiProject_Weather_observations/en/SPARQL#Request_for_update)), write the result in file quickstatements.txt:
```bash
 python3 msc_weather_stations_to_wikidata.py  -o quickstatements.txt
```
<br />

Use local files for stations and write the output in file quickstatements.txt
```bash
 python3 msc_weather_stations_to_wikidata.py -f station_list/Station\ Inventory\ FR.csv -e station_list/Station\ Inventory\ EN.csv   -o quickstatements.txt
```
<br />

Use local files for stations, CSV file _element_stations.csv_ for elements already in Wikidata, print results only for station Q59495897 (Active Pass) and for property P31 (instance of)
```bash
 python3 msc_weather_stations_to_wikidata.py  -f station_list/Station\ Inventory\ FR.csv -e station_list/Station\ Inventory\ EN.csv -w element_stations.csv -q Q59495897 -p 131
```
<br />

Update only the information of stations existing in CSV file _element_stations.csv_ :
```bash
 python3 msc_weather_stations_to_wikidata.py  -f station_list/Station\ Inventory\ FR.csv -e station_list/Station\ Inventory\ EN.csv -w element_stations.csv -q all 
```
<br />

Update all the information of every station, disregarding if it is already present or not in WD, existing in CSV file _element_stations.csv_ :
```bash
 python3 msc_weather_stations_to_wikidata.py  -f station_list/Station\ Inventory\ FR.csv -e station_list/Station\ Inventory\ EN.csv -w element_stations.csv -q all -p all
```
<br />

Remove P31 properties for every station listed in CSV file _element_stations.csv_ :
```bash
 python3 msc_weather_stations_to_wikidata.py  -f station_list/Station\ Inventory\ FR.csv -e station_list/Station\ Inventory\ EN.csv -w element_stations.csv -q all -p 31 --delete
```
<br />



Results
-----

Map of all weather stations in Wikidata before the import:
![WD map with WX station before MSC stations are added](images/map_before_msc_stations.png )


Map of all weather stations on Wikidata after the import:
![WD map with WX station after MSC stations are added](images/map_after_msc_stations.png)

Bugs
-----

For any bug report, please contact [msc_weather_stations_to_wikidata.miguel@ptaff.ca](mailto:msc_weather_stations_to_wikidata.miguel@ptaff.ca)

Author
-----

[Miguel Tremblay](http://ptaff.ca/miguel/)

License
-----

Copyright © 2018 Miguel Tremblay.

msc_weather_stations_to_wikidata is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.