#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright  2018  Miguel Tremblay

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not see  <http://www.gnu.org/licenses/>.
############################################################################

"""
Name:        msc_constant.py
Description:  Constant values used in msc_weather_stations_to_wikidata.py

Notes: 

Author: Miguel Tremblay (http://ptaff.ca/miguel/)
Date: January 17th 2019 (Happy Birthday Mom!)
"""

import datetime

VERSION = "0.1"
# Verbose level:
## 1 Normal mode
## 2 Full debug
NORMAL= 1
VERBOSE= 2

nGlobalVerbosity = 1

# WD date format when only the year is provided
sDATE_END_YEAR_FORMAT = '-01-01T00:00:00Z/09'
NOW = datetime.datetime.now()
sCURRENT_YEAR = str(NOW.year)
sLAST_CIVIL_YEAR  = str(NOW.year - 1)

# URLs
MSC_WEBSITE_URL = "http://climate.weather.gc.ca/"
MSC_FTP_URL = "ftp://client_climate@ftp.tor.ec.gc.ca/Pub/Get_More_Data_Plus_de_donnees/"
STATION_LIST_EN = MSC_FTP_URL + "Station%20Inventory%20EN.csv"
STATION_LIST_FR = MSC_FTP_URL + "Répertoire%20des%20stations%20FR.csv"
dSTATION_URL = {"FR" : STATION_LIST_FR, \
               "EN" : STATION_LIST_EN}

# CSV file station list
MSC_COLUMN_TITLE_EN=["Name","Province","Climate ID","Station ID","WMO ID",\
                     "TC ID","Latitude (Decimal Degrees)", \
                     "Longitude (Decimal Degrees)", "Latitude","Longitude", \
                     "Elevation (m)","First Year","Last Year", \
                     "HLY First Year","HLY Last Year","DLY First Year", \
                     "DLY Last Year", "MLY First Year","MLY Last Year"]

# CSV element file
COLUMN_ELEMENT = ["station", "label_en", "description_en", "label_fr", \
                  "description_fr", "instance_of", "coordinate_location",\
                  "provinceLabel", "identifiant_WIGOS_de_station", \
                  "property_of", "start_date", "end_date", "msc_climate_id",\
                  "altitude", "data_interval", "hourly", "h_start", "h_end", \
                  "daily", "d_start", "d_end", "monthly", "m_start", "m_end"]

dPROPERTIES = { "Lfr" : "French element name",
                "Dfr" : "French description",
                "Len" : "English element name",
                "Den" : "English description",
                "17": "country",
                "31" : "instance of",
                "127": "property of",
                "131" : "located in the administrative territorial entity (Province/Territory)",
                "625" :"coordinate location",
                "729": "service entry",
                "2044" : "elevation above sea level",
                "3999": "date of official closure",
                "4136": "WIGOS station ID",
                "6242": "Meteorological Service of Canada climate site ID",
                "6339": "Data interval (HLY, DLY, MLY)",
                "all" : "All properties above"}

# Correspondance between QuickStatement and CSV file column title
dPROPERTIES_ELEMENT = { "Lfr": "label_fr",
                        "Dfr": "description_fr", 
                        "Len": "label_en", 
                        "Den": "description_en", 
                        "31" : "instance_of",
                        "127": "property_of",
                        "625": "coordinate_location",
                        "729": "start_date",
                        "2044": "altitude",
                        "3999": "end_date",
                        "4136": "identifiant_WIGOS_de_station",
                        "6242": "msc_climate_id",
                        "6339": "data_interval",
                        "HLY" : "hourly",
                        "HLY_end" : "h_end",
                        "DLY" : "daily",
                        "DLY_end": "d_end",
                        "MLY" : "monthly",
                        "MLY_end" : "m_end"}

## ID for data interval
dINTERVAL = { "HLY": "Q59657010",
              "DLY": "Q59657036",
              "MLY": "Q59657037",
              "CLI": "Q59657033"}

# Wikidata properties to set
sQS_HEADER = "CREATE"
sCOUNTRY = "P17\tQ16" # Country is Canada
sPROPERTY_OF = "P127\tQ349450" # Property of MSC
# P31 ('instance of') for manual and automatic station
sHEADER_MAN = 'P31\tQ190107'
sHEADER_AUTO =  'P31\tQ846837'
sFOOTER_FR = '\tS854\t"' +  STATION_LIST_FR + '"\tS407\tQ150\tS813\t+'
sFOOTER_EN = '\tS854\t"' + STATION_LIST_EN + '"\tS407\tQ1860\tS813\t+'
sINSTANCE_MAN_STATION_FR = sHEADER_MAN + sFOOTER_FR
sINSTANCE_AUTO_STATION_FR = sHEADER_AUTO +  sFOOTER_FR
sINSTANCE_MAN_STATION_EN = sHEADER_MAN + sFOOTER_EN
sINSTANCE_AUTO_STATION_EN = sHEADER_AUTO + sFOOTER_EN
sURL_SEARCH_HEADER_FR = '\tS854\t"' + "http://climate.weather.gc.ca/historical_data/search_historic_data_stations_f.html?searchType=stnName&txtStationName="
sURL_SEARCH_FOOTER_FR = "&searchMethod=contains&optLimit=yearRange&StartYear=1840&EndYear=" + sCURRENT_YEAR  + "&Year=" + sCURRENT_YEAR + "&Month=" + \
               str(NOW.month) + "&Day=" + str(NOW.day) + '"\tS407\tQ150'



########## Provinces and territories #################

dPROV_ELEMENT = {"ALBERTA":"Q1951",
                "COLOMBIE-BRITANNIQUE":"Q1974",
                "ILE DU PRINCE-EDOUARD":"Q1979",
                "MANITOBA":"Q1948",
                "NOUVEAU-BRUNSWICK":"Q1965",
                "NOUVELLE-ECOSSE":"Q1952",
                "NUNAVUT":"Q2023",
                "ONTARIO":"Q1904",
                "QUEBEC":"Q176",
                "SASKATCHEWAN":"Q1989",
                "TERRE-NEUVE":"Q2003",
                "TERRITOIRES DU NORD-OUEST":"Q2007",
                "YUKON":"Q2009"}

dPROV_FR = {"ALBERTA":"Alberta",
                "COLOMBIE-BRITANNIQUE":"Colombie-Britannique",
                "ILE DU PRINCE-EDOUARD":"Île-du-Prince-Édouard",
                "MANITOBA":"Manitoba",
                "NOUVEAU-BRUNSWICK":"Nouveau-Brunswick",
                "NOUVELLE-ECOSSE":"Nouvelle-Écosse",
                "NUNAVUT":"Nunavut",
                "ONTARIO":"Ontario",
                "QUEBEC":"Québec",
                "SASKATCHEWAN":"Saskatchewan",
                "TERRE-NEUVE":"Terre-Neuve-et-Labrador",
                "TERRITOIRES DU NORD-OUEST":"Territoire du Nord-Ouest",
                "YUKON":"Yukon"}

dPROV_EN = {"ALBERTA":"Alberta",
                "COLOMBIE-BRITANNIQUE":"British-Columbia",
                "ILE DU PRINCE-EDOUARD":"Prince Edward Island",
                "MANITOBA":"Manitoba",
                "NOUVEAU-BRUNSWICK":"New Brunswick",
                "NOUVELLE-ECOSSE":"Nova Scotia",
                "NUNAVUT":"Nunavut",
                "ONTARIO":"Ontario",
                "QUEBEC":"Québec",
                "SASKATCHEWAN":"Saskatchewan",
                "TERRE-NEUVE":"Newfoundland and Labrador",
                "TERRITOIRES DU NORD-OUEST":"North-Western Territory",
                "YUKON":"Yukon"}

                                
### Station name: some string must be lower, other upper

lLOWER = [" Des ", " Aux ", " And ", "'S", " De ", "'L ", "-Aux-", "D'", \
          " La ", " Au ", "-Des-", " Du ", "-De-", " A la", "-Du-", " L'", \
          " Of "]
lUPPER = [" Ubc", " Bc", " Awos", " Bcfs", " Unb", " Cs", " Cda Fe", " Rcs", \
          " Nbepc", " Ihd", " Hq", " Ccr", " Sdu", " Epf", " Nsac", \
          " Nrc", " Dnd", " Cba", " Cda", "Io", " Fes", " Cfam", " Ua", " Rs", \
          " Cfb", " Gs", " Tcpl ", " Ww" ]
