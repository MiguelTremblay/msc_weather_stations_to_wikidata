#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright  2019  Miguel Tremblay

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not see  <http://www.gnu.org/licenses/>.
############################################################################

"""
Name:        msc_weather_stations_to_wikidata.py
Description:  Python3 script parsing the list of Meteorological Service of Canada (MSC) 
 weather stations into Wikidata input format (QuickStatements).

Notes: 

Author: Miguel Tremblay (http://ptaff.ca/miguel/)
Date: November 29th 2018
"""

import sys
import os
import csv
import urllib.error
import urllib.request
import io

from msc_constant import *

def my_print(sMessage, nMessageVerbosity=NORMAL):
   """
   Use this method to write the message in the standart output 
   """

   if nMessageVerbosity == NORMAL:
      print (sMessage)
   elif nMessageVerbosity == VERBOSE and nGlobalVerbosity == VERBOSE:
      print (sMessage)

def print_properties():
   """
   Print the number and description of each property included in the 
   dictoniary dPROPERTIES.
   """

   for sKey in dPROPERTIES.keys():
      print (sKey + "\t", dPROPERTIES[sKey])

def overwrite_property(lProperties, sProperty):
   """
   Check if the property sProperty is in the list given in argument lProperties.

   Return True if yes, or if 'all' is given.
   """

   if lProperties == None:
      return False 
   elif sProperty in lProperties or 'all' in lProperties:
      return True
   else:
      return False
      
def is_automatic_station(sStationName):
   """
   Return True if the station is automatic, False otherwise.
   """

   # instance of : check if this is an automated station
   if sStationName[-2:] == " A" or sStationName[-5:] == '(AUT)' or \
      sStationName[-5:] == " AUTO":
      return True
   else:
      return False
      
def get_station_name(sName):
   """
   Remove the "A" at the end of the string if it exists (automatic station).
   """

   if is_automatic_station(sName):
      if sName[-2:] == "A":
         sName = sName[:-2]
      elif sName[-5:] == '(AUT)' or sName[-5:] == ' AUTO':
         sName = get_station_name(sName[:-5])

   sName = get_stn_proper_case(sName)
   return sName

def get_stn_proper_case(sStnName):
   """
   Transform the station name by:
   1- Keep only the first letter of each words in uppercase
   2- Transform some words in lower or upper case
   """

   sName = sStnName.title()

   # Uppercase (Ex: Cs -> CS)
   for sUpper in lUPPER:
      sName = sName.replace(sUpper, sUpper.upper())

   # Lowercase (Ex: Des -> des)
   for sLower in lLOWER:
      sName = sName.replace(sLower, sLower.lower())

   return sName

def write_description(bStationInWikiData, sProperty, \
                      dStationWD=None, lPropertiesToOV=None):
   """
   Check if the property sProperty should be defined. 
   
   The property should be defined if:
   1- it is not already defined in WD OR
   2- the item exits in WD but is empty OR
   3- the user ask to overwrite this specific property
   """
   if not bStationInWikiData:
      return True
   elif bStationInWikiData:
      sPropertyInWD = dStationWD[dPROPERTIES_ELEMENT[sProperty]]
      if sPropertyInWD == "":
         return True
         
   if overwrite_property(lPropertiesToOV, sProperty):
      return True

   return False

def get_description_lang(sStation, sProvince, sID, sLang):
   """
   Return the description sentence, based on the language and if it's an automated 
   station or not.
   """
   sAutomatic = ""
   if sLang == "FR":

      # If location is provided but not in prov/terr list,
      ## use the location directly.
      ##  Ex: Saguenay for YBG
      if sProvince in dPROV_FR.keys():
         sLocationFr = dPROV_FR[sProvince]
      else:
         sLocationFr = sProvince

      if is_automatic_station(sStation):
         sStation = get_station_name(sStation)
         sAutomatic = " automatique"

      sDescription = "station météorologique" +sAutomatic  +\
                     " du Service météorologique du Canada pour " +\
                     sStation + ' (ID SMC: ' + sID + '), ' + sLocationFr + ', Canada'

   elif sLang == "EN":
      # If location is provided but not in prov/terr list,
      ## use the location directly.
      ##  Ex: Saguenay for YBG
      if sProvince in dPROV_EN.keys():
         sLocationEn = dPROV_EN[sProvince]
      else:
         sLocationEn = sProvince

      if is_automatic_station(sStation):
         sStation = get_station_name(sStation)
         sAutomatic = " automatic"

      sDescription = "Meteorological Service of Canada's" + sAutomatic +\
                     " station for " + sStation + ' (MSC ID: ' + sID  +\
                     '), ' + sLocationEn + ', Canada' 

   return sDescription


def check_msc_climate_connexion():
   """
   Check if we can connect the MSC Climate web site. 
   If not, there is point to continue.
   """

   my_print("Checking if MSC Climate web site is available...", \
            nMessageVerbosity=VERBOSE)

   try:
      urllib.request.urlopen(MSC_WEBSITE_URL)
   except urllib.error.URLError :
      my_print ("ERROR: Climate web site not available", nMessageVerbosity=NORMAL)
      my_print ("Check your internet connexion or try to reach\n '" +\
                MSC_WEBSITE_URL + "'\n in a web browser.",
                nMessageVerbosity=NORMAL)
      my_print ("Exiting.", nMessageVerbosity=NORMAL)
      sys.exit(1)

   my_print("MSC Climate web site reached! Continuing. ",\
            nMessageVerbosity=VERBOSE)

   
def load_station_list(sPath, sLang):
   """
   Load the station file from the disk or online. 

   Return the CSVDict with the information, and the modified date of the file
   written on the first line in the format 'YYYY-MM-DD'.
   """
   
   dStationList = {}

   # Check if a local path is given
   if sPath is not None:
      my_print("Loading local file for station list at: " +\
               sPath, nMessageVerbosity=VERBOSE)
      # Open file
      if os.path.exists(sPath) == False:
         my_print("ERROR: Local station path does not exist: " +sPath,\
                  nMessageVerbosity=NORMAL)
         my_print("Please fix this error or try the online version of station file.")
         my_print("Exiting")
         exit(2)
      else:
         file_list = open(sPath, 'r', encoding="utf-8-sig")
         station_list = csv.DictReader(file_list, fieldnames=\
                                       MSC_COLUMN_TITLE_EN)
   else:
      # Check if we can contact MSC web site
      check_msc_climate_connexion()
      try:
         my_print("Loading online station list at: " + \
                  dSTATION_URL[sLang], nMessageVerbosity=VERBOSE)
         my_print("This may take a while...", nMessageVerbosity=VERBOSE)         
         # Recipe from http://bit.ly/2hc9XMB
         webpage = urllib.request.urlopen(dSTATION_URL[sLang])
         ## Convert to utf-8

         station_list = csv.DictReader(io.TextIOWrapper(webpage, encoding='utf-8'), \
                                       fieldnames=MSC_COLUMN_TITLE_EN)

      except urllib.error.URLError :
         my_print ("ERROR: Online CSV station list not available.", \
                   nMessageVerbosity=NORMAL)
         my_print ("Cannot reach the FTP web site.\n",\
                   nMessageVerbosity=NORMAL)
         my_print ("This can be caused by firewall settings or by the FTP site being unreachable.\n Try accessing the URL through a web browser:\n '" +\
                    dSTATION_URL[sLang]+ "'\n", nMessageVerbosity=NORMAL)
         my_print ("If not working, the FTP server may be experiencing down time.\n",\
                    nMessageVerbosity=NORMAL) 
         my_print ("Local station list not provided.\n You can try using a local version provided with get_canadian_weather_observations.py with '-S' arguments in command line. Exiting.",\
                      nMessageVerbosity=NORMAL)
         exit(9)

   # Date information is on the first line of the station file
   if sLang == "FR": # Ex: Date de modification: 2018-12-02 01:33 UTC
      nIndice = 3
   elif sLang == "EN": # Ex: Modified Date: 2018-12-02 01:33 UTC
      nIndice = 2

   # Print date in QuickStatement format: 2019-01-01T23:33
   # QuickStatement has day as the smallest interval. Ignore HH:MM of the file.
   sDate = next(station_list)["Name"].split()[nIndice] + 'T00:00:00Z/11'
   # Skip the next 3 lines
   for i in range(3):
      next(station_list)
         
   return (station_list, sDate)


def get_element_description(sNameFr, sNameEn, sProvTerr, bStationInWD, \
                            dStationInWD, lPropertiesOv, sMSC_ID):
   """
   Get label and description: Lfr, Dfr, Len, Den
   """

   lDescription = []
   
   ### French  ###
   # Description
   if write_description(bStationInWD, "Lfr", dStationInWD, lPropertiesOv): 
      lDescription.append('Lfr\t"' + sNameFr + '"')
   if write_description(bStationInWD, "Dfr", dStationInWD, lPropertiesOv): 
      sDescriptionFr = get_description_lang(sNameFr, sProvTerr, sMSC_ID, "FR")
      lDescription.append('Dfr\t"' +  sDescriptionFr + '"')

   ### English  ###
   # Name
   if write_description(bStationInWD, "Len", dStationInWD, lPropertiesOv): 
      lDescription.append('Len\t"' + sNameEn + '"')

   # Description
   if write_description(bStationInWD, "Den", dStationInWD, lPropertiesOv): 
      sDescriptionEn = get_description_lang(sNameEn, sProvTerr, sMSC_ID, "EN")
      lDescription.append('Den\t"' + sDescriptionEn + '"')

   return lDescription

def get_station_type(sNameFr, bStationInWD, dStationInWD, lPropertiesOv): 
   """
   Return the type of station: P31
   """

   lStation = []

   if write_description(bStationInWD, "31", dStationInWD, lPropertiesOv):
      ### Type of Wx station ###
      if is_automatic_station(sNameFr):
         lStation.append(sHEADER_AUTO)
      else:
         lStation.append(sHEADER_MAN)
      
   return lStation

def get_altitude(dStationInfoFr, sProvTerr, bStationInWD, dStationInWD, \
                 lPropertiesOv):
   """
   Get the altitude.
   """
   lAltitude = []
   fAltitudeMSC = None
   sAltitudeMSC = dStationInfoFr['Elevation (m)']
   # Some altitudes are written ".6"
   if len(sAltitudeMSC) > 0 and sAltitudeMSC[0] == ".": 
      sAltitudeMSC = "0" + sAltitudeMSC
   if sAltitudeMSC != "":
      fAltitudeMSC = round(float(sAltitudeMSC),2)
      
   # Check if the value has changed
   if bStationInWD:
      sAltitudeWD = dStationInWD[dPROPERTIES_ELEMENT["2044"]]
      if sAltitudeWD != "":
         fAltitudeWD = round(float(sAltitudeWD),2)
         if fAltitudeMSC != fAltitudeWD:
            my_print("Altitude must be updated.\nValues\tMSC:" +sAltitudeMSC +\
                     "\tWD:" + sAltitudeWD,  nMessageVerbosity=VERBOSE)
            # Delete altitude
            lAltitude.append("-P2044\t" +sAltitudeWD )
            # Reset 
            lAltitude = lAltitude + \
                        get_altitude(dStationInfoFr, sProvTerr, False,\
                                     dStationInWD, lPropertiesOv)
   
   if write_description(bStationInWD, "2044", dStationInWD, lPropertiesOv):
      if sAltitudeMSC != "": # Altitude not provided for every station
         lAltitude.append("P2044\t" + sAltitudeMSC + "U11573")
         if bStationInWD:
            my_print("Altitude updated.\nValues\tMSC:" +sAltitudeMSC +\
                     "\tWD:" + sAltitudeWD, nMessageVerbosity=VERBOSE)


   return lAltitude

def get_latlon(dStationInfoFr, sProvTerr, bStationInWD, dStationInWD, \
                 lPropertiesOv):
   """
   Get lat/lon.

   Lat/lon in decimal degrees have a precision of 2 digits in MSC list.
   """

   lLatLon = []
   sLatMSC = dStationInfoFr['Latitude (Decimal Degrees)']
   fLatMSC = round(float(sLatMSC),2)
   sLonMSC = dStationInfoFr['Longitude (Decimal Degrees)']
   fLonMSC = round(float(sLonMSC),2)

   if bStationInWD:
      # WD format is "Point(sLon, sLat)"
      sLatLonWD = dStationInWD[dPROPERTIES_ELEMENT["625"]]
      if sLatLonWD != "":
         sLonWD = sLatLonWD.split()[0].split('(')[1]
         fLonWD = round(float(sLonWD),2)
         sLatWD = sLatLonWD.split()[1].split(')')[0]
         fLatWD = round(float(sLatWD),2)
         # Cast in float because MSC lat/lon values are sometimes integer
         if fLatMSC != fLatWD or \
            fLonMSC != fLonWD:
            my_print("Lat/lon must be updated.\nLat\tMSC:" + sLatMSC +\
                     "\tWD:" + sLatWD + "\nLon:" + sLonMSC + "\tWD:" + \
                     sLonWD,  nMessageVerbosity=VERBOSE)

            # Delete altitude
            lLatLon.append("-P625\t@" + sLatWD + "/" + sLonWD)
            # Reset 
            lLatLon = lLatLon +  get_latlon(dStationInfoFr, sProvTerr, \
                                            False, dStationInWD, \
                                            lPropertiesOv)
         

   if write_description(bStationInWD, "625", dStationInWD, lPropertiesOv):
      # coordinate location
      sLat = dStationInfoFr['Latitude (Decimal Degrees)']
      sLon = dStationInfoFr['Longitude (Decimal Degrees)']
      if sLat != "0" and sLon != "0" : # Some stations have 0/0 as lat/lon...
         lLatLon.append("P625\t@" + sLat + "/" + sLon )

   return lLatLon
         
def get_location(dStationInfoFr, sProvTerr, bStationInWD, dStationInWD, \
                 lPropertiesOv):
   """
   Get the location information: P625, P2044, P131, P17
   """

   lLocation = []
   
   ### Location ###
   # lat/lon
   lLocation = lLocation + get_latlon(dStationInfoFr, sProvTerr, \
                                      bStationInWD, dStationInWD, \
                                      lPropertiesOv)

   # Altitude
   lLocation = lLocation + get_altitude(dStationInfoFr, sProvTerr, \
                                        bStationInWD, dStationInWD, \
                                        lPropertiesOv)
         

   ## Country 
   if not bStationInWD or overwrite_property(lPropertiesOv, "17"):
      ### Country :: hardcoded to Canada (P17 == Q16)
      lLocation.append(sCOUNTRY)
      
   ### located in the administrative territorial entity ###
   if sProvTerr in dPROV_ELEMENT.keys() and \
      not bStationInWD or overwrite_property(lPropertiesOv, "131"):
      lLocation.append("P131\t" + dPROV_ELEMENT[sProvTerr] )

   return lLocation


def get_wigos(dStationInfoFr, bStationInWD, dStationInWD, lPropertiesOv):
   """ 
   Get WIGOS station number: P4136
   """

   lWigos = []
   ### WIGOS ID ###
   sWigos = dStationInfoFr['WMO ID']
   
   if  len(sWigos) > 0 and \
       write_description(bStationInWD, "4136", dStationInWD, lPropertiesOv):
      lWigos.append('P4136\t"0-20000-0-' + sWigos + '"')

   return lWigos

def get_service_date(dStationInfoFr, bStationInWD, dStationInWD, lPropertiesOv):
   """
   Get Service entry and Closure: P729, P3999
   """

   lStartEnd = []
   
   sDateStart = dStationInfoFr['First Year']
   if write_description(bStationInWD, "729", dStationInWD, lPropertiesOv):
      lStartEnd.append('P729\t+' +\
                       sDateStart  + sDATE_END_YEAR_FORMAT)

   sDateLast =  dStationInfoFr['Last Year']
   # Do not write if last year is current
   if len(sDateLast) > 0 or \
      overwrite_property(lPropertiesOv, "3999"):
      if sDateLast != sCURRENT_YEAR:
         if not bStationInWD or dStationInWD["end_date"] == "":
            lStartEnd.append('P3999\t+' +\
                             sDateLast + sDATE_END_YEAR_FORMAT)
      # If station is in WD but closure date is last year, and it is updated
      #  to current year (a common fix by MSC)
      elif bStationInWD:
         sLastYearWD = dStationInWD[dPROPERTIES_ELEMENT["3999"]][:4]
         if sLastYearWD != "" and int(sLastYearWD) + 1 == int(sCURRENT_YEAR):
            my_print("Date of official closure must be removed from WD.", \
                     nMessageVerbosity=VERBOSE)
            lStartEnd.append('-P3999\t+' +sLastYearWD + sDATE_END_YEAR_FORMAT )

   return lStartEnd


def get_msc_climate_id(dStationInfoFr, bStationInWD, \
                       dStationInWD, lPropertiesOv, sDateModifEn, sDateModifFr):
   """
   Get MSC Climate ID: P6242
   """
      
   nClimateID = dStationInfoFr['Climate ID']
   lClimateID = []
   
   if write_description(bStationInWD, "6242", dStationInWD, lPropertiesOv):

      sQS = 'P6242\t"' + nClimateID + '"'  + sFOOTER_EN + sDateModifEn
      sQS2  = 'P6242\t"' + nClimateID + '"' + sFOOTER_FR + sDateModifFr
      # If the station is already in WD, write the FR ref
      lClimateID.append(sQS)
      lClimateID.append(sQS2)

   return lClimateID

def get_property_of_msc(sFrName, sEnName, bStationInWD,\
                        dStationInWD, lPropertiesOv):
   """
   Set property 127 ('property of') to sPROPERTY_OF.
   Add a URL to MSC station search, using FR and EN name
   into the URL to retrieve the targated station.

   """
   lProperty = []
   
   if write_description(bStationInWD, "127", dStationInWD, lPropertiesOv):

      # FR
      sURL_fr = sURL_SEARCH_HEADER_FR + sFrName.replace(" ", "+") +\
                sURL_SEARCH_FOOTER_FR
      sProperty = sPROPERTY_OF + sURL_fr
      lProperty.append(sProperty)
      
      # EN
      # Create the EN URL by switching "_f" by "_e"
      sURL_en = sURL_fr.replace("stations_f.html", "stations_e.html")
      # and by replacing Q150 (French) by Q1860 (English_
      sURL_en = sURL_en.replace("Q150", "Q1860")
      sProperty = sPROPERTY_OF + sURL_en      
      lProperty.append(sProperty)

   return lProperty


def get_data_interval(dStationInfoFr, bStationInWD, \
                      dStationInWD, lPropertiesOv, \
                      sIntervalRequest=None):
   """
   Check what is the data interval (P6339) for this station:
   1- Hourly (Q59657010)
   2- Daily (Q59657036)
   3- Monthly (Q59657037)
   4- TODO: Climate (Q59657033)

   For each type, write the date at which the data started to be recorded,
   as well as the end if it's not the current year:
   A- Start Time (P580)
   B- End Time (P582)
   """

   lDataInterval = []
   lInterval = ['HLY', 'DLY', 'MLY']
   if sIntervalRequest !=None:
      lInterval = [sIntervalRequest]
   ## Check if station has Hourly data interval
   for sInterval in lInterval:
      if write_description(bStationInWD, sInterval, \
                           dStationInWD, lPropertiesOv):
         sKeyStart = sInterval + ' First Year' 
         sStartYear = dStationInfoFr[sKeyStart]
         if sStartYear != "":
            sIntervalQS = 'P6339\t' + dINTERVAL[sInterval] +\
                          '\tP580\t+' + sStartYear +\
                          sDATE_END_YEAR_FORMAT
            sKeyEnd = sInterval + ' Last Year' 
            sYearEnd = dStationInfoFr[sKeyEnd]
            # Add only if last year is not the current one
            if sYearEnd != "" and sYearEnd != sCURRENT_YEAR:
               sIntervalQS = sIntervalQS + "\tP582\t+" +\
                             sYearEnd + sDATE_END_YEAR_FORMAT
            lDataInterval.append(sIntervalQS)

   # If last year of interval is previous civil year
   #  and info from MSC indicates current
   #  year, erase the information from WD (it's a MSC fix in its list)
   if bStationInWD:
      bReset = False
      for sInterval in ['HLY', 'DLY', 'MLY']:
         # Check if there is value in WD
         sKeyEndTimeInterval = sInterval + ' Last Year' 
         sLastYearMSC = dStationInfoFr[sKeyEndTimeInterval]
         if dStationInWD[dPROPERTIES_ELEMENT[sInterval]] != "":
            # Check if there is a last year
            sLastYearWD =dStationInWD[dPROPERTIES_ELEMENT[sInterval + "_end"]]
            if sLastYearWD != "":
               sLastYearWD = sLastYearWD[0:4] # Keep only the 4 digits for YYYY
               sKeyEnd = sInterval + ' Last Year' 
               sYearEndMSC = dStationInfoFr[sKeyEnd]
            # Check if it's a fix from MSC, updating last year for current year
            if sLastYearWD == sLAST_CIVIL_YEAR and \
               sYearEndMSC == sCURRENT_YEAR:
               my_print("End time must be removed for:" +sInterval,
                        nMessageVerbosity=VERBOSE)
               # In this case, erase the property and start again
               sRemoveDataInterval = "-P6339\t" +  dINTERVAL[sInterval] 
               lDataInterval.append(sRemoveDataInterval)
               lNewInterval = get_data_interval(dStationInfoFr, False, \
                                                dStationInWD, lPropertiesOv,
                                                sInterval)
               lDataInterval = lDataInterval + lNewInterval
            elif sLastYearMSC != sLastYearWD and sLastYearMSC !=sCURRENT_YEAR:
               my_print("End time updated for period:" +sInterval +\
                        "\nValue:\tMSC:" +sLastYearMSC + "\tWD:" + sLastYearWD,
                        nMessageVerbosity=VERBOSE)
               # In this case, erase the property and start again
               sRemoveDataInterval = "-P6339\t" +  dINTERVAL[sInterval] 
               lDataInterval.append(sRemoveDataInterval)
               lNewInterval = get_data_interval(dStationInfoFr, False, \
                                                dStationInWD, lPropertiesOv,
                                                sInterval)
               lDataInterval = lDataInterval + lNewInterval
               

               
   return lDataInterval

def get_line(sStart, sStatement, lPropertyOV, bDelete):
   """
   Concatenation of the start of the line (CREATE or Q code) and the QS 
   instructions. Add "-" in front of properties asked for deletion.
   """

   # If first character is '-', then line must be deleted
   if sStatement[0] == '-':
      sStatement = sStatement[1:]
      sStart = '-' + sStart

   sConcat = sStart + sStatement

   if lPropertyOV != None and bDelete:
      sElement = sStatement.split()[0]
      for sProperty in lPropertyOV:
         if sElement == 'P' + sProperty:
            sConcat = "-" + sConcat

   return sConcat


def fill_quick_statement(dStationInfoFr, dStationInfoEn, dElementExisting, \
                         tOptions, sDateFr, sDateEn):
   """
   This is where the work is done.

   Create the QuickStatement based on the information included in both
   French and English information line (dictionnary based on CSV information)
   """

   lStationRequested = tOptions.ElementList
   lPropertiesOverwrite= tOptions.PropertyList
   bDelete = tOptions.bDelete
   # Label and description from MSC file
   sNameFr  = get_stn_proper_case(dStationInfoFr['Name'])
   sNameEn = get_stn_proper_case(dStationInfoEn['Name'])
   sClimateID = dStationInfoEn['Climate ID']

   # Beginning of QS section for this station
   sComment = "#### " + sNameFr + " ####"
   lStatement = [sComment]
   
   # Station in WD
   if dElementExisting != None and sClimateID in dElementExisting.keys():
      dStationInWD = dElementExisting[sClimateID]
      sElement =  dStationInWD['element'] 
      # Check if the elment is in requested list (if any)
      if lStationRequested != None and sElement not in lStationRequested and \
         'all' not in lStationRequested:
         return []
      # Otherwise load the station
      else:
         bStationInWD = True
         sQSStartLine = sElement + '\t'
         sProvTerr = dStationInfoFr['Province'] 
   # Brand new station           
   elif lStationRequested == None :
      lStatement.append(sQS_HEADER)
      bStationInWD = False
      dStationInWD = None
      sQSStartLine = "LAST\t"
      sProvTerr = dStationInfoFr['Province']
   else:
      return []

   # Description (Lfr, Dfr, Len, Den)
   lStatement = lStatement + get_element_description(sNameFr, sNameEn, sProvTerr, \
                                                     bStationInWD, dStationInWD,\
                                                     lPropertiesOverwrite, \
                                                     dStationInfoFr['Climate ID'])
   # Instance of (P31)
   lStatement = lStatement + get_station_type(sNameFr, bStationInWD, \
                                              dStationInWD, lPropertiesOverwrite)
   # Lat/Lon/altitude/Country/Administrative
   lStatement = lStatement + get_location(dStationInfoFr, sProvTerr,\
                                          bStationInWD, dStationInWD, lPropertiesOverwrite)
   # WIGOS 
   lStatement = lStatement + get_wigos(dStationInfoFr, bStationInWD, \
                                       dStationInWD, lPropertiesOverwrite)  

   ### Property of (P127)
   lStatement = lStatement + get_property_of_msc(sNameFr, sNameEn, \
                                                 bStationInWD, \
                                                 dStationInWD, \
                                                 lPropertiesOverwrite)

   ### Service entry and Closure: P729, P3999
   lStatement = lStatement + get_service_date(dStationInfoFr, bStationInWD,\
                                              dStationInWD, lPropertiesOverwrite)

   ### Get climate ID: P6242
   lStatement = lStatement + get_msc_climate_id(dStationInfoFr, bStationInWD,\
                                                dStationInWD, \
                                                lPropertiesOverwrite, sDateEn, sDateFr)


   ### Get the data interval for this station
   lStatement = lStatement + get_data_interval(dStationInfoFr, bStationInWD,\
                                           dStationInWD, lPropertiesOverwrite)

   
   # If the only line is the comment, there is no declaration
   if lStatement[-1] == sComment:
      lStatement = lStatement[:-1]
   else:
      # If it is a new item, start at the second line
      if lStatement[1] == sQS_HEADER : # New item
         my_print("New station", nMessageVerbosity=VERBOSE)
         nStart = 2
      else: # Item already in WD
         nStart = 1
         # For the update of the reference date in P6242
         lStatement.append('-P6242\t"' + dStationInfoFr['Climate ID'] + '"')
         lStatement = lStatement +\
                      get_msc_climate_id(dStationInfoFr, False,\
                                       dStationInWD, \
                                       lPropertiesOverwrite, sDateEn, sDateFr)


      # Write the name of the station at the end in debug mode
      my_print(sComment, nMessageVerbosity=VERBOSE)
      for i in range(nStart,len(lStatement)):
         sLine = get_line(sQSStartLine, lStatement[i],lPropertiesOverwrite, \
                          bDelete )
         lStatement[i] = sLine
         
      # This empty line separate each station info
      lStatement.append("")

   return lStatement


def open_wd_file(sPath, lElementList):
   """
   Open the CSV file export from Wikidata containing the
    information already filled in  Wikidata for this station.
   Return a dictionnary of dictionnaries.
   """
   
   my_print("Loading element file at: " + sPath, nMessageVerbosity=VERBOSE)
   # Open file
   if os.path.exists(sPath) == False:
      my_print("ERROR: Element file path does not exist: " +sPath,\
               nMessageVerbosity=NORMAL)
      my_print("Please fix this error or try the online version of station file.")
      my_print("Exiting")
      exit(2)
   else:
      file_element = open(sPath, 'r', encoding="utf-8")
      element_list = csv.DictReader(file_element, fieldnames=COLUMN_ELEMENT)

   # Create a dictionnary of dictionaries, using the name of the station
   #  in capital letters as key.
   # This key is used to match the station name in MSC's list.
   next(element_list)
   dElement = {}
   lElementCSV = []
   for row in element_list:
      sKey = row['msc_climate_id']
      sElement = row['station'].rsplit('/')[-1]
      lElementCSV.append(sElement)
      dElement[sKey] = {'element': sElement}
      for sColumn in COLUMN_ELEMENT[1:] :
         dElement[sKey][sColumn] = row[sColumn]

   # If a list of element is provided, check if it is included in the CSV file.
   if lElementList != None:
      for sElement in lElementList:
         if sElement in lElementCSV or lElementList[0] == 'all':
            return dElement
         else:
            print("ERROR: element not listed in CSV file:", sElement)
            print("Please provide a valid element number.")
            print("Values included in the file are:")
            print('\n'.join(set(lElementCSV)))
            exit(6)
         
   return dElement


      
def msc_weather_station_to_wikidata(tOptions):
   """
   Python3 script parsing the list of MSC weather stations into 
   Wikidata input format (QuickStatements).
   """

   # Load file with existing element
   dictReaderElement = {}
   if tOptions.ElementStationPath != None:
      dElementStation = open_wd_file(tOptions.ElementStationPath, \
                                          tOptions.ElementList)
   else:
      dElementStation =  None

   # Load the station list
   (stationFrList, sDateFileFr) = load_station_list(tOptions.LocalStationPathFR,\
                                                    "FR")
   (stationEnList, sDateFileEn) =  load_station_list(tOptions.LocalStationPathEN,\
                                                     "EN")

   # Load a Quick Statement block for each station
   lTextQuickStatement = []
   for rowFr in stationFrList:
      rowEn = next(stationEnList)
      lCurrentStatement = fill_quick_statement(rowFr, rowEn, dElementStation, \
                                               tOptions,\
                                               sDateFileFr, sDateFileEn)
      lTextQuickStatement = lTextQuickStatement + lCurrentStatement

   if tOptions.OutputFile != None:
         fichier = open(tOptions.OutputFile,  "w", encoding="utf-8")
         fichier.writelines([sString+os.linesep \
                             for sString in lTextQuickStatement])
         fichier.close()
   else:
      print(*lTextQuickStatement, sep='\n')


def check_file_writable(fnm):
   """
   Copied from 
   https://www.novixys.com/blog/python-check-file-can-read-write/#3_Checking_if_file_can_be_written
   """

   if os.path.exists(fnm):
      # path exists
      if os.path.isfile(fnm): # is it a file or a dir?
         # also works when file is a link and the target is writable
         return os.access(fnm, os.W_OK)
      else:
         return False # path is a dir, so cannot write as a file
   # target does not exist, check perms on parent dir
   pdir = os.path.dirname(fnm)
   if not pdir: pdir = '.'
   # target is creatable if parent dir is writable
   return os.access(pdir, os.W_OK)


############################################################
# msc_weather_stations_to_wikidata in Command line
#
#

import argparse

def get_command_line():
   """
   Parse the command line and perform all the checks.
   """

   parser = argparse.ArgumentParser(prog='PROG', prefix_chars='-',\
                                    description="Python3 script parsing the list of MSC weather stations into Wikidata input format (QuickStatements).")
   parser.add_argument("--output-file", "-o", dest="OutputFile", \
                     help="Location where the file will be written in QuickStatement. Default value is where the script get_canadian_weather_observations.py is located.",\
                     action="store", type=str, default=None)
   parser.add_argument("--online", "-N", dest="bOnline", \
                       help="Download the online version of the MSC file.",\
                       action="store_true", default=False)
   parser.add_argument("--station-file-fr", "-f", dest="LocalStationPathFR", \
                     help="Use this local version located at PATH for the station list instead of the online French version on the EC Climate web site.",\
                     action="store", type=str, default=None)
   parser.add_argument("--station-file-en", "-e", dest="LocalStationPathEN", \
                     help="Use this local version located at PATH for the station list instead of the online English version on the EC Climate web site.",\
                     action="store", type=str, default=None)
   parser.add_argument("--wikidata-file", "-w", dest="ElementStationPath", \
                     help="When a station is listed in this file, use the element number instead of creating a new element.",\
                     action="store", type=str, default=None)
   parser.add_argument("--overwrite-property", "-p", dest="PropertyList", nargs="+", \
                     help="Overwrite the value(s) of element(s) given in argument, even if there is already a value provided in the Elements file. This option can only be used with the option --overwrite. If the value 'all' is provided, all the properties are overwritten.",\
                     action="store", default=None)
   parser.add_argument("--station-only", "-q", dest="ElementList", nargs="+", \
                       help="Provide values only for stations identified by their elements values (Q1, Q2, etc.). If the value 'all' is provided, only the stations existing in the Elements file will be provided in the output. This option can only be used with the option --overwrite.",\
                     action="store", default=None)
   parser.add_argument("--delete", "-d", dest="bDelete", \
                       help="Delete the property listed with -p in the QS instruction.",\
                       action="store_true", default=False)
   
   parser.add_argument("--list-properties", "-l", dest="PrintProperties",  \
                       help="List all the property values assigned by this script and their corresponding description, then exit.",
                       action="store_true", default=False)

   parser.add_argument("--verbose", "-v", dest="Verbosity", \
                     help="Explain what is being done", action="store_true", default=False)
   parser.add_argument("--version", "-V", dest="bVersion", \
                       help="Output version information and exit",\
                       action="store_true", default=False)               

   # Parse the args
   options = parser.parse_args()

         
   # --list-properties
   if options.PrintProperties:
      print_properties()
      exit(0)

   # Check if using local or online MSC station file
   if not options.bOnline and not options.LocalStationPathFR:
      print ("Please specify if you want to use local (-e -f) or online (-N) MSC station file")
      exit(0)
   elif options.bOnline:
      print ("Info: downloading the online version of the station list on MSC web site.")
      print("This can take some time...")
      

   # --overwrite-property
   if options.PropertyList != None:
      if options.ElementStationPath == None:
         print ("ERROR: Option --overwrite-property (-p) must be used with the option --wikidata-file (-w). Please provide a valid CSV file with the option -w.")
         exit(2)
      else:
         for sProperty in options.PropertyList:
            if sProperty not in dPROPERTIES.keys():
               print ("Property not in list:", sProperty)
               print ("Please provide a property in the following list")
               print_properties()
               exit(4)

   # --station-only
   if options.ElementList != None and options.ElementStationPath == None:
      print ("ERROR: Option --station-only (-q) must be used with the option --wikidata-file (-w). Please provide a valid CSV file with the option -w.")
      exit(2)

   # --delete valid only with -p
   if options.bDelete and options.PropertyList == None:
      print ("ERROR: Option --delete (-d) is used only on the property listed with the option --overwrite-property (-p). Please provide a list using -p or remove -d.")
      exit (5)
      
      
   # --version
   if options.bVersion:
      print ("msc_weather_stations_to_wikidata.py version: " + VERSION)
      print ("Copyright (C) 2017 Free Software Foundation, Inc.")
      print ("License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.")
      print ("This is free software: you are free to change and redistribute it.")
      print ("There is NO WARRANTY, to the extent permitted by law.\n")
      print ("Written by Miguel Tremblay, http://ptaff.ca/miguel/")
      exit(0)
   
   # Verify it the output is a directory
   if options.OutputFile is not None and not \
      check_file_writable(options.OutputFile):
      print ("Error: File '%s' provided in '--output-file' or '-o' does not exist or is not writable. Please provide a valid output file path. Exiting." %\
             (options.OutputFile))
      exit (3)

   # Set the global verbosity
   global nGlobalVerbosity
   if options.Verbosity:
      nGlobalVerbosity = VERBOSE
   else:
      nGlobalVerbosity = NORMAL
      
   my_print("Verbosity level is set to: " + str(nGlobalVerbosity), \
            nMessageVerbosity=VERBOSE)
   my_print("Arguments in command line are:\n " + str(sys.argv), \
            nMessageVerbosity=VERBOSE)
   
   return options


if __name__ == "__main__":

   tOptions = get_command_line()
   msc_weather_station_to_wikidata(tOptions)
